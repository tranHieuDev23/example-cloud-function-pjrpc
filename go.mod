module hello-world

go 1.21

require (
	github.com/GoogleCloudPlatform/functions-framework-go v1.8.0
	github.com/cloudevents/sdk-go/v2 v2.14.0
	github.com/go-playground/validator/v10 v10.17.0
	github.com/google/uuid v1.3.0
	github.com/google/wire v0.5.0
	gitlab.com/pjrpc/pjrpc/v2 v2.5.0
)

require (
	cloud.google.com/go/functions v1.15.1 // indirect
	github.com/gabriel-vasile/mimetype v1.4.2 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/leodido/go-urn v1.2.4 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v0.0.0-20180701023420-4b7aa43c6742 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0 // indirect
	golang.org/x/crypto v0.9.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
