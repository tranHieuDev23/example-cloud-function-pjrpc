.PHONY: generate
generate:
	rm -rf internal/handlers/http/rpc/rpcclient
	rm -rf internal/handlers/http/rpc/rpcserver

	go get gitlab.com/pjrpc/pjrpc/cmd/genpjrpc@v0.4.0
	go get github.com/google/wire/cmd/wire@v0.5.0
	
	go generate ./...

	go mod tidy

.PHONY: run-serve-api
run-serve-api:
	PORT=8080 FUNCTION_TARGET=serveAPI go run cmd/main.go

.PHONY: run-serve-consumer
run-serve-consumer:
	PORT=8090 FUNCTION_TARGET=serveConsumer go run cmd/main.go