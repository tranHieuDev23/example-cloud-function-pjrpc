package handlers

import (
	"hello-world/internal/handlers/events"
	"hello-world/internal/handlers/http"

	"github.com/google/wire"
)

var WireSet = wire.NewSet(
	http.WireSet,
	events.WireSet,
)
