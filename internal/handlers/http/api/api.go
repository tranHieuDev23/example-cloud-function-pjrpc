package api

type ErrorCode int

const (
	ErrorCodeOK                 ErrorCode = 1
	ErrorCodeCanceled           ErrorCode = 2
	ErrorCodeUnknown            ErrorCode = 3
	ErrorCodeInvalidArgument    ErrorCode = 4
	ErrorCodeDeadlineExceeded   ErrorCode = 5
	ErrorCodeNotFound           ErrorCode = 6
	ErrorCodeAlreadyExists      ErrorCode = 7
	ErrorCodePermissionDenied   ErrorCode = 8
	ErrorCodeResourceExhausted  ErrorCode = 9
	ErrorCodeFailedPrecondition ErrorCode = 10
	ErrorCodeAborted            ErrorCode = 11
	ErrorCodeOutOfRange         ErrorCode = 12
	ErrorCodeUnimplemented      ErrorCode = 13
	ErrorCodeInternal           ErrorCode = 14
	ErrorCodeUnavailable        ErrorCode = 15
	ErrorCodeDataLoss           ErrorCode = 16
	ErrorCodeUnauthenticated    ErrorCode = 17
)

//go:generate genpjrpc -search.name=API -print.place.path_swagger_file=../../../../api/swagger.json -print.content.swagger_data_path=../../../../configs/genpjrpc_swagger.json
type API interface {
	GetGreeting(GetGreetingRequest) GetGreetingResponse
}

type GetGreetingRequest struct {
	Thing string `validate:"alphanum,max=32"`
}

type GetGreetingResponse struct {
	Greeting string
}
