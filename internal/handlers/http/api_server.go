package http

import (
	"context"
	"fmt"

	"hello-world/internal/handlers/http/api"
	"hello-world/internal/handlers/http/api/rpcserver"

	validator "github.com/go-playground/validator/v10"
)

type APIServer struct {
	validate *validator.Validate
}

func NewAPIServer() rpcserver.APIServer {
	return &APIServer{
		validate: validator.New(),
	}
}

func (s APIServer) GetGreeting(
	ctx context.Context,
	in *api.GetGreetingRequest,
) (*api.GetGreetingResponse, error) {
	return &api.GetGreetingResponse{
		Greeting: fmt.Sprintf("Hello, %s!", in.Thing),
	}, nil
}
