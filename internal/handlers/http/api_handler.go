package http

import (
	"context"
	"net/http"

	"hello-world/internal/handlers/http/api/rpcserver"

	"gitlab.com/pjrpc/pjrpc/v2"
)

func NewAPIHandler(
	apiServer rpcserver.APIServer,
	middlewareList []pjrpc.Middleware,
) http.Handler {
	srv := pjrpc.NewServerHTTP()
	srv.OnPanic = func(ctx context.Context, err error) *pjrpc.ErrorResponse {
		return srv.DefaultRestoreOnPanic(ctx, err)
	}

	rpcserver.RegisterAPIServer(srv, apiServer, middlewareList...)

	return srv
}
