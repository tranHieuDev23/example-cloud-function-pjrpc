package http

import (
	"github.com/google/wire"
	"gitlab.com/pjrpc/pjrpc/v2"
)

func InitializePJRPCMiddlewareList() []pjrpc.Middleware {
	return []pjrpc.Middleware{}
}

var WireSet = wire.NewSet(
	NewAPIServer,
	NewAPIHandler,
	InitializePJRPCMiddlewareList,
)
