package events

import (
	"context"
	"log"

	"hello-world/internal/handlers/events/api"
	"hello-world/internal/handlers/events/api/rpcserver"

	validator "github.com/go-playground/validator/v10"
)

type APIServer struct {
	validate *validator.Validate
}

func NewAPIServer() rpcserver.APIServer {
	return &APIServer{
		validate: validator.New(),
	}
}

func (s APIServer) OnHello(ctx context.Context, in *api.OnHelloEvent) {
	log.Println(in.Greeting)
}
