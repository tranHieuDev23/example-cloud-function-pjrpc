package api

//go:generate genpjrpc -search.name=API -print.place.path_swagger_file=""
type API interface {
	OnHello(OnHelloEvent)
}

type OnHelloEvent struct {
	Greeting string `validate:"alphanum,max=32"`
}
