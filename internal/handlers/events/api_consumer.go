package events

import (
	"context"

	"hello-world/internal/handlers/events/api/rpcserver"

	v2 "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/pjrpc/pjrpc/v2"
)

type Consumer struct {
	*pjrpc.Server
}

func NewAPIConsumer(
	apiServer rpcserver.APIServer,
) *Consumer {
	consumer := &Consumer{
		Server: pjrpc.NewServer(),
	}

	rpcserver.RegisterAPIServer(consumer, apiServer)

	return consumer
}

func (c Consumer) HandleEvent(ctx context.Context, e v2.Event) error {
	if e.DataMediaType() != pjrpc.ContentTypeHeaderValue {
		return pjrpc.JRPCErrInvalidRequest("content-type must be 'application/json'")
	}

	c.Serve(pjrpc.ContextSetData(ctx, &pjrpc.ContextData{}), e.Data())

	return nil
}
