//go:build wireinject
// +build wireinject

//
//go:generate go run github.com/google/wire/cmd/wire
package wiring

import (
	"net/http"

	"hello-world/internal/handlers"
	"hello-world/internal/handlers/events"

	"github.com/google/wire"
)

var WireSet = wire.NewSet(
	handlers.WireSet,
)

func InitializeHTTPAPIHandler() http.Handler {
	wire.Build(WireSet)

	return nil
}

func InitializeEventAPIConsumer() *events.Consumer {
	wire.Build(WireSet)

	return nil
}
