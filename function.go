package function

import (
	"context"
	"net/http"

	"hello-world/internal/wiring"

	"github.com/GoogleCloudPlatform/functions-framework-go/functions"
	v2 "github.com/cloudevents/sdk-go/v2"
)

func init() {
	httpAPIHandler := wiring.InitializeHTTPAPIHandler()
	eventAPIConsumer := wiring.InitializeEventAPIConsumer()
	functions.HTTP("serveAPI", func(w http.ResponseWriter, r *http.Request) {
		httpAPIHandler.ServeHTTP(w, r)
	})
	functions.CloudEvent("serveConsumer", func(ctx context.Context, e v2.Event) error {
		return eventAPIConsumer.HandleEvent(ctx, e)
	})
}
